% $$FileInfo
% $Filename: mnk.m
%
% Copyright (c) 2014-2015 Jelena Banjac.
%

function [ X, Td ] = mnk( satellitesNo, matOfVisSatelites )
%Input arguments:
%   satellitesNo - Number of visible satellites
%   matOfVisSatelites - Matrix of coordinates of visible satellites
%
N = 10; %broj iteracija
C = 3*10^8;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Podaci 4 satelita pomocu kojih odredjujemo poziciju resivera
% Omoguceno je da broj satelita bude i veci, a za sada je testiran sa 4.
% Podaci bi se citali iz neke datoteke
%satellitesNo = 4;

D = matOfVisSatelites(:,4);
A = matOfVisSatelites(:,[1 2 3]);
A = A';


% d1 = 1022228206.42;
% x1 = 7766188.44;
% y1 = -21960535.34;
% z1 = 12522838.56;
% X1 = [x1 y1 z1]';
% A(:,1) = X1;
% D(1) = d1;
%
% d2 = 1024096139.11;
% x2 = -25922679.66;
% y2 = -6629461.28;
% z2 = 31864.37;
% X2 = [x2 y2 z2]';
% A(:,2) = X2;
% D(2) = d2;
%
% d3 = 1021729070.63;
% x3 = -5743774.02;
% y3 = -25828319.92;
% z3 = 1692757.72;
% X3 = [x3 y3 z3]';
% A(:,3) = X3;
% D(3) = d3;
%
% d4 = 1021259581.09;
% x4 = -2786005.69;
% y4 = -15900725.8;
% z4 = 21302003.49;
% X4 = [x4 y4 z4]';
% A(:,4) = X4;
% D(4) = d4;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

x = 0; y = 0; z = 0;
X = [x y z]';

ctb = 0; delX = 0; delctb = 0;

%MNK
for j = 1:N
    X = X + delX;
    ctb = ctb + delctb;
    for i=1:satellitesNo
        r(i) = ((X - A(:,i))' * (X - A(:,i)))^.5;
    end

    R = [];
    for i=1:satellitesNo
        R = [R; r(i) - (D(i)+ctb)];
    end
    %error = -[r1-(d1+ctb); r2-(d2+ctb); r3-(d3+ctb); r4-(d4+ctb)];
    error = -R;

    Grad = [];
%   grad1 = [-(1/r1)*(A(:,1)- X)' -1];
%   grad2 = [-(1/r2)*(A(:,2)- X)' -1];
%   grad3 = [-(1/r3)*(A(:,3)- X)' -1];
%   grad4 = [-(1/r4)*(A(:,4)- X)' -1];
%   Grad = [grad1; grad2; grad3; grad4];
    for i=1:satellitesNo
        Grad = [Grad; -(1/r(i))*(A(:,i)-X)' -1];
    end
    Gradinv = (inv(Grad' * Grad)) * Grad';

    K = 0.5;
    %xi = inv(Ai'*Ai)*Ai'bi  --- Ai -> Grad, bi -> error
    delX = K * [1 0 0 0; 0 1 0 0; 0 0 1 0] * Gradinv * error;
    delctb = K * [0 0 0 1] * Gradinv * error;
end

%Koordinate resivera
%X
%Kasnjenje sata
Td = ctb/C;

end
