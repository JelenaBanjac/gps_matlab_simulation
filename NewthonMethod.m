% $$FileInfo
% $Filename: NewthonMethod.m
%
% Copyright (c) 2014-2015 Jelena Banjac.
%

function [ Xnewthon ] = NewthonMethod( xstart,ystart,zstart,dstart,sateliteNumber,matOfVisSatelites )
    N = 10; %broj iteracija
    %c = 3*10^8;
    %Xnewthon = [];
    %xstart = 0; ystart = 0; zstart = 0; dstart = 0;
    Xnewthon = [xstart; ystart; zstart; dstart];
    for j=1:N
        [Xnewthon, Fun] = Newton(Xnewthon(1),Xnewthon(2),Xnewthon(3),Xnewthon(4),sateliteNumber, matOfVisSatelites);

        n = size(Fun);
        df = 0;
        for k=1:n
            df = df + Fun(k);
        end
        L(j) = df;

    end

    %greska vremena
    %f(4)/c

end
